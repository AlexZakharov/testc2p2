using Microsoft.Extensions.DependencyInjection;
using TestC2P2.Common.Helpers.Classes;
using TestC2P2.Common.Helpers.Interfaces;
using TestC2P2.DataAccess.UnitOfWork.Classes;
using TestC2P2.DataAccess.UnitOfWork.Interfaces;
using TestC2P2.Services.Classes;
using TestC2P2.Services.Interfaces;
using TestC2P2.Services.Validators.Classes;
using TestC2P2.Services.Validators.Interfaces;

namespace TestC2P2.API.StartUps
{
    public class DependencyInjectionStartUp
    {
        public static void Inject(IServiceCollection services)
        {
            services.AddScoped<ITransactionService, TransactionService>();
            
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddTransient<IFileParserTransactionFabric, FileParserTransactionFabric>();
            services.AddTransient<ICsvTransactionFileParser, CsvTransactionFileParser>();
            services.AddTransient<IXmlTransactionFileParser, XmlTransactionFileParser>();
            
            services.AddTransient<IStatusHelper, StatusHelper>();
            services.AddTransient<IFileFormatHelper, FileFormatHelper>();
            services.AddTransient<ITransactionFileValidator, TransactionFileValidator>();
            services.AddTransient<ITransactionViewModelValidator, TransactionViewModelValidator>();
        }
    }
}