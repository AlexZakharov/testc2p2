﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestC2P2.Common.Enums;
using TestC2P2.Common.Helpers.Interfaces;
using TestC2P2.Common.Models.ApiResponse;
using TestC2P2.Common.Models.Dto;
using TestC2P2.Services.Interfaces;
using TestC2P2.Services.Models.Input;
using TestC2P2.Services.Validators.Interfaces;

namespace TestC2P2.API.Controllers
{
    [Route("api/[controller]")]
    public class TransactionController : ControllerBase
    {
        private readonly ITransactionService _transactionService;
        private readonly IMapper _mapper;
        private readonly IFileFormatHelper _fileFormatHelper;
        private readonly ITransactionFileValidator _transactionFileValidator;

        public TransactionController(
            ITransactionService transactionService,
            IMapper mapper,
            IFileFormatHelper fileFormatHelper,
            ITransactionFileValidator transactionFileValidator)
        {
            _transactionService = transactionService;
            _mapper = mapper;
            _fileFormatHelper = fileFormatHelper;
            _transactionFileValidator = transactionFileValidator;
        }

        [HttpPost("ImportTransactionFromFile")]
        public async Task<IActionResult> ImportTransactionFromFile(IFormFile file)
        {
            var transactionFileValidatorResult =  _transactionFileValidator
                .Validate(new TransactionFileValidatorInput { FileContentType = file.ContentType, FileLength = file.Length });

            if (transactionFileValidatorResult.Failed)
            {
                return this.BadRequest(new ApiBadRequestResponse(transactionFileValidatorResult.ErrorMessages));
            }

            var fileType = _fileFormatHelper.GetFileTypeByInputSting(file.ContentType);
            
            var importTransactionFromFileInput = new ImportTransactionFromFileInput { File = file, FileTypeEnum = fileType };
            var importTransactionFromFileOutput = await _transactionService.ImportTransactionFromFile(importTransactionFromFileInput).ConfigureAwait(false);
            
            if (importTransactionFromFileOutput.Failed)
            {
                return this.BadRequest(new ApiBadRequestResponse(importTransactionFromFileOutput.ErrorMessages));
            }
            
            return this.Ok();
        }

        [HttpGet]
        public async Task<IActionResult> Get(string currency, DateTime? startDate, DateTime? endDate, StatusEnum? status)
        {
            var getTransactionInput = new GetTransactionsInput{ Currency = currency, StartDate = startDate, EndDate = endDate, Status = status };
            var getTransactionsOutput = await _transactionService.GetTransactions(getTransactionInput).ConfigureAwait(false);

            if (getTransactionsOutput.Failed)
            {
                return this.BadRequest(new ApiBadRequestResponse(getTransactionsOutput.ErrorMessages));
            }

            var transactionsDto = _mapper.Map<List<TransactionDto>>(getTransactionsOutput.Output.Transactions);
            return this.Ok(new ApiOkResponse(transactionsDto));
        }
    }
}