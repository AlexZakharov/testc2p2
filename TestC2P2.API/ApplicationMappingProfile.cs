﻿using System.Globalization;
using AutoMapper;
using TestC2P2.Common.Models.Dto;
using TestC2P2.Common.Models.Transaction;
using TestC2P2.DataAccess.Entities;
using TestC2P2.Services.Models.Input;

namespace TestC2P2.API
{
    public class ApplicationMappingProfile : Profile
    {
        public ApplicationMappingProfile()
        {
            this.CreateMap<GetTransactionsInput, TransactionSearchModel>();

            this.CreateMap<Transaction, TransactionDto>()
                .ForMember(destinationMember => destinationMember.Id,
                    memberOptions => memberOptions.MapFrom(sourceMember => sourceMember.TransactionId))
                .ForMember(destinationMember => destinationMember.Payment,
                    memberOptions => memberOptions.MapFrom(sourceMember =>
                        $"{sourceMember.Amount.ToString(CultureInfo.InvariantCulture)} {sourceMember.CurrencyCode}"))
                .ForMember(destinationMember => destinationMember.Status,
                    memberOptions => memberOptions.MapFrom(sourceMember => sourceMember.Status.ToString()));

            this.CreateMap<TransactionViewModel, Transaction>();
        }
    }
}