﻿using System;

namespace TestC2P2.Common.Models.Parser
{
    public class CsvTransactionReadModel
    {
        public string TransactionId { get; set; }
        
        public decimal? Amount { get; set; }
        
        public string CurrencyCode { get; set; }
        
        public DateTime? TransactionDate { get; set; }
        
        public string Status { get; set; }
    }
}