﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace TestC2P2.Common.Models.ApiResponse
{
    public class ApiBadRequestResponse : ApiResponse
    {
        public ApiBadRequestResponse(string errorMessage)
            : base(StatusCodes.Status400BadRequest)
        {
            this.ErrorMessages.Add(errorMessage);
        }

        public ApiBadRequestResponse(IEnumerable<string> errorMessages)
            : base(StatusCodes.Status400BadRequest)
        {
            this.ErrorMessages.AddRange(errorMessages);
        }

        public ApiBadRequestResponse(ModelStateDictionary modelState)
            : base(StatusCodes.Status400BadRequest)
        {
            var errorMessages = modelState.SelectMany(modelStateEntry => modelStateEntry.Value.Errors).Select(modelError => modelError.ErrorMessage);
            this.ErrorMessages.AddRange(errorMessages);
        }
    }
}