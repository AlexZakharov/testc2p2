﻿namespace TestC2P2.Common.Models.ApiResponse
{
    public class ApiModelResponse : ApiResponse
    {
        public object Model { get; }

        public ApiModelResponse(object model, int statusCode) : base(statusCode)
        {
            this.Model = model;
        }
    }
}