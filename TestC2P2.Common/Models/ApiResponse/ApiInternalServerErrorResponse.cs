﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace TestC2P2.Common.Models.ApiResponse
{
    public class ApiInternalServerErrorResponse : ApiResponse
    {
        public ApiInternalServerErrorResponse(string errorMessage)
            : base(StatusCodes.Status500InternalServerError)
        {
            this.ErrorMessages.Add(errorMessage);
        }

        public ApiInternalServerErrorResponse(IEnumerable<string> errorMessages)
            : base(StatusCodes.Status500InternalServerError)
        {
            this.ErrorMessages.AddRange(errorMessages);
        }
    }
}