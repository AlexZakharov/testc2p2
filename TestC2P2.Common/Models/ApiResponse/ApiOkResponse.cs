﻿﻿using Microsoft.AspNetCore.Http;

 namespace TestC2P2.Common.Models.ApiResponse
{
    public class ApiOkResponse : ApiModelResponse
    {
        public ApiOkResponse(object model)
            : base(model, StatusCodes.Status200OK)
        {
        }
    }
}