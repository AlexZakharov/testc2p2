﻿﻿using System.Collections.Generic;
 using Microsoft.AspNetCore.Http;
 using Newtonsoft.Json;

 namespace TestC2P2.Common.Models.ApiResponse
{
    public class ApiResponse
    {
        public int StatusCode { get; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Message { get; }

        public List<string> ErrorMessages { get; }

        public ApiResponse(int statusCode)
        {
            this.StatusCode = statusCode;
            this.Message = GetMessage(statusCode);
            this.ErrorMessages = new List<string>();
        }

        public ApiResponse(int statusCode, List<string> errorMessages)
        {
            this.StatusCode = statusCode;
            this.Message = GetMessage(statusCode);
            this.ErrorMessages = errorMessages;
        }

        private static string GetMessage(int statusCode)
        {
            string message;

            switch (statusCode)
            {
                case StatusCodes.Status200OK:
                    message = "The request has succeeded.";

                    break;
                case StatusCodes.Status400BadRequest:
                    message = "The request could not be understood by the server due to malformed syntax.";

                    break;
                
                case StatusCodes.Status500InternalServerError:
                    message = "The server encountered an unexpected condition which prevented it from fulfilling the request.";

                    break;
                default:
                    message = string.Empty;

                    break;
            }

            return message;
        }
    }
}