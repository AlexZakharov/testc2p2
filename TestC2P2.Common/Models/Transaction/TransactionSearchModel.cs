﻿using System;
using System.ComponentModel.DataAnnotations;
using TestC2P2.Common.Enums;

namespace TestC2P2.Common.Models.Transaction
{
    public class TransactionSearchModel
    {
        [StringLength(3)]
        public string Currency { get; set; }
        
        public DateTime? StartDate { get; set; }
        
        public DateTime? EndDate { get; set; }
        
        public StatusEnum? Status { get; set; }
    }
}