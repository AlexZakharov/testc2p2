﻿using System;
using TestC2P2.Common.Enums;

namespace TestC2P2.Common.Models.Transaction
{
    public class TransactionViewModel
    {
        public string TransactionId { get; set; }
        
        public decimal? Amount { get; set; }
        
        public string CurrencyCode { get; set; }
        
        public DateTime? TransactionDate { get; set; }
        
        public StatusEnum? Status { get; set; }
    }
}