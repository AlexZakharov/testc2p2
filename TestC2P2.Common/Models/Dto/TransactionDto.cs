﻿namespace TestC2P2.Common.Models.Dto
{
    public class TransactionDto
    {
        public string Id { get; set; }
        
        public string Payment { get; set; }
        
        public string Status { get; set; }
    }
}