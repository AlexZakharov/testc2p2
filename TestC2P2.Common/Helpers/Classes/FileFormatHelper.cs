﻿using TestC2P2.Common.Enums;
using TestC2P2.Common.Helpers.Interfaces;

namespace TestC2P2.Common.Helpers.Classes
{
    public class FileFormatHelper : IFileFormatHelper
    {
        public SupportedFileTypesEnum? GetFileTypeByInputSting(string fileType)
        {
            SupportedFileTypesEnum? supportedFileTypesEnum;

            switch (fileType)
            {
                case "text/csv":
                case "application/octet-stream":
                    supportedFileTypesEnum = SupportedFileTypesEnum.Csv;
                    break;
                case "application/xml":
                case "text/xml":
                    supportedFileTypesEnum = SupportedFileTypesEnum.Xml;
                    break;
                default:
                    supportedFileTypesEnum = null;
                    break;
            }

            return supportedFileTypesEnum;
        }
    }
}