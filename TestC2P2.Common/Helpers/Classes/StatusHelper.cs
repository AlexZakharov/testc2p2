﻿using TestC2P2.Common.Enums;
using TestC2P2.Common.Helpers.Interfaces;

namespace TestC2P2.Common.Helpers.Classes
{
    public class StatusHelper : IStatusHelper
    {
        public StatusEnum? GetStatusEnumByInputSting(string inputString)
        {
            StatusEnum? statusEnum;

            switch (inputString)
            {
                case "Approved":
                    statusEnum = StatusEnum.A;
                    break;
                case "Failed":
                case "Rejected":
                    statusEnum = StatusEnum.R;
                    break;
                case "Finished":
                case "Done":
                    statusEnum = StatusEnum.D;
                    break;
                default:
                    statusEnum = null;
                    break;
            }

            return statusEnum;
        }
    }
}