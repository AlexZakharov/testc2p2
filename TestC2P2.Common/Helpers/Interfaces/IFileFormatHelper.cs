﻿using TestC2P2.Common.Enums;

namespace TestC2P2.Common.Helpers.Interfaces
{
    public interface IFileFormatHelper
    {
        SupportedFileTypesEnum? GetFileTypeByInputSting(string fileType);
    }
}