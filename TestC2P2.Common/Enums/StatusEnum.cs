﻿namespace TestC2P2.Common.Enums
{
    public enum StatusEnum
    {
        A = 1, // Approved
        R = 2, // Failed or Rejected
        D = 3 // Finished or Done
    }
}