﻿namespace TestC2P2.Common.Enums
{
    public enum SupportedFileTypesEnum
    {
        Csv = 1,
        Xml = 2
    }
}