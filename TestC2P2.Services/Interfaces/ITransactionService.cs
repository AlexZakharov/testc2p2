﻿using System.Threading.Tasks;
using TestC2P2.Services.Models.Input;
using TestC2P2.Services.Models.Output;

namespace TestC2P2.Services.Interfaces
{
    public interface ITransactionService
    {
        Task<ServiceOutput<ImportTransactionFromFileOutput>> ImportTransactionFromFile(ImportTransactionFromFileInput importTransactionFromFileInput);
        Task<ServiceOutput<GetTransactionsOutput>> GetTransactions(GetTransactionsInput getTransactionsInput);
    }
}