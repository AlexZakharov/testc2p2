﻿using TestC2P2.Common.Enums;

namespace TestC2P2.Services.Interfaces
{
    public interface IFileParserTransactionFabric
    {
        ITransactionFileParser GetTransactionFileParser(SupportedFileTypesEnum? fileType);
    }
}