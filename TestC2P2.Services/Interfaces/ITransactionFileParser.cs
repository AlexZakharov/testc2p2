﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using TestC2P2.Common.Models.Transaction;

namespace TestC2P2.Services.Interfaces
{
    public interface ITransactionFileParser
    {
        IEnumerable<TransactionViewModel> ParseFile(IFormFile file);
    }
}