﻿using TestC2P2.Services.Models.Input;
using TestC2P2.Services.Models.Output;

namespace TestC2P2.Services.Validators.Interfaces
{
    public interface ITransactionViewModelValidator
    {
        ServiceOutput<TransactionViewModelValidatorOutput> Validate(TransactionViewModelValidatorInput transactionViewModelValidatorInput);
    }
}