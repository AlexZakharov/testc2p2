﻿using TestC2P2.Services.Models.Input;
using TestC2P2.Services.Models.Output;

namespace TestC2P2.Services.Validators.Interfaces
{
    public interface ITransactionFileValidator
    {
        ServiceOutput<TransactionFileValidatorOutput> Validate(TransactionFileValidatorInput transactionFileValidatorInput);
    }
}