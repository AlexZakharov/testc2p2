﻿using System;
using System.Collections.Generic;
using System.Linq;
using TestC2P2.Services.Models.Input;
using TestC2P2.Services.Models.Output;
using TestC2P2.Services.Validators.Interfaces;

namespace TestC2P2.Services.Validators.Classes
{
    public class TransactionViewModelValidator : ITransactionViewModelValidator
    {
        private const string WrongFormatOrEmptyErrorMessage = "Wrong data or empty";
        
        public ServiceOutput<TransactionViewModelValidatorOutput> Validate(TransactionViewModelValidatorInput transactionEntityValidatorInput)
        {
            var errors = new List<string>();

            var modelToValidate = transactionEntityValidatorInput.TransactionViewModel;

            if (string.IsNullOrEmpty(modelToValidate.TransactionId) || modelToValidate.TransactionId.Length > 50)
            {
                errors.Add($"TransactionId: {WrongFormatOrEmptyErrorMessage}");
            }

            if (modelToValidate.Amount == null || modelToValidate.Amount == 0)
            {
                errors.Add($"Amount: {WrongFormatOrEmptyErrorMessage}");
            }
            
            if (string.IsNullOrEmpty(modelToValidate.CurrencyCode) || modelToValidate.CurrencyCode.Length > 3)
            {
                errors.Add($"CurrencyCode: {WrongFormatOrEmptyErrorMessage}");
            }

            if (modelToValidate.TransactionDate == null || modelToValidate.TransactionDate == new DateTime())
            {
                errors.Add($"TransactionDate: {WrongFormatOrEmptyErrorMessage}");
            }

            if (modelToValidate.Status == null)
            {
                errors.Add($"Status: {WrongFormatOrEmptyErrorMessage}");
            }
            
            var transactionEntityValidatorOutput = errors.Any() 
                ? ServiceOutput<TransactionViewModelValidatorOutput>.Fail(errors) 
                : ServiceOutput<TransactionViewModelValidatorOutput>.Success();

            return transactionEntityValidatorOutput;
        }
    }
}