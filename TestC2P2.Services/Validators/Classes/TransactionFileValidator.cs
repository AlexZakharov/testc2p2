﻿using System.Collections.Generic;
using System.Linq;
using TestC2P2.Services.Models.Input;
using TestC2P2.Services.Models.Output;
using TestC2P2.Services.Validators.Interfaces;

namespace TestC2P2.Services.Validators.Classes
{
    public class TransactionFileValidator : ITransactionFileValidator
    {
        private const long SupportedFileSizeInByte = 1000000; // 1MB
        private const string UnknownFormatErrorMessage = "Unknown format";
        private const string FileSizeErrorMessage = "File size is to large";
        
        public ServiceOutput<TransactionFileValidatorOutput> Validate(TransactionFileValidatorInput transactionFileValidatorInput)
        {
            var errors = new List<string>();
            
            var formats = new List<string>
            {
                "text/csv",
                "application/octet-stream",
                "application/xml",
                "text/xml"
            };

            if (!formats.Contains(transactionFileValidatorInput.FileContentType))
            {
                errors.Add(UnknownFormatErrorMessage);
            }

            if (transactionFileValidatorInput.FileLength >= SupportedFileSizeInByte)
            {
                errors.Add(FileSizeErrorMessage);
            }

            var transactionFileValidatorOutput = errors.Any() 
                ? ServiceOutput<TransactionFileValidatorOutput>.Fail(errors) 
                : ServiceOutput<TransactionFileValidatorOutput>.Success();

            return transactionFileValidatorOutput;
        }
    }
}