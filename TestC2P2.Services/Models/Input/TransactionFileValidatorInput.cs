﻿namespace TestC2P2.Services.Models.Input
{
    public class TransactionFileValidatorInput
    {
        public string FileContentType { get; set; }
        public long FileLength { get; set; }
    }
}