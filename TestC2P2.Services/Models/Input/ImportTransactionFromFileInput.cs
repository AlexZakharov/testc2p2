﻿using Microsoft.AspNetCore.Http;
using TestC2P2.Common.Enums;

namespace TestC2P2.Services.Models.Input
{
    public class ImportTransactionFromFileInput
    {
        public IFormFile File { get; set; }
        
        public SupportedFileTypesEnum? FileTypeEnum { get; set; }
    }
}