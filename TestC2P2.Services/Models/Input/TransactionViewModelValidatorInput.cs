﻿using TestC2P2.Common.Models.Transaction;
using TestC2P2.DataAccess.Entities;

namespace TestC2P2.Services.Models.Input
{
    public class TransactionViewModelValidatorInput
    {
        public TransactionViewModel TransactionViewModel { get; set; }
    }
}