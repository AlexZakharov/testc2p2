﻿using System.Collections.Generic;
using System.Linq;
using TestC2P2.Services.Models.Output.Base;

namespace TestC2P2.Services.Models.Output
{
    public class ServiceOutput<TServiceOutputBase> where TServiceOutputBase : ServiceOutputBase
    {
        protected ServiceOutput()
        {
        }

        public bool Failed => this.ErrorMessages.Any();
        public bool Succeeded => !this.Failed;

        public TServiceOutputBase Output { get; set; }

        public List<string> ErrorMessages { get; } = new List<string>();

        public static ServiceOutput<TServiceOutputBase> Success()
        {
            var serviceOutput = new ServiceOutput<TServiceOutputBase>();

            return serviceOutput;
        }

        public static ServiceOutput<TServiceOutputBase> Success(TServiceOutputBase output)
        {
            var serviceOutput = new ServiceOutput<TServiceOutputBase> {Output = output};

            return serviceOutput;
        }

        public static ServiceOutput<TServiceOutputBase> Fail(string errorMessage)
        {
            var serviceOutput = new ServiceOutput<TServiceOutputBase>();
            serviceOutput.ErrorMessages.Add(errorMessage);

            return serviceOutput;
        }

        public static ServiceOutput<TServiceOutputBase> Fail(string errorMessage, TServiceOutputBase output)
        {
            var serviceOutput = new ServiceOutput<TServiceOutputBase> {Output = output};
            serviceOutput.ErrorMessages.Add(errorMessage);

            return serviceOutput;
        }

        public static ServiceOutput<TServiceOutputBase> Fail(IEnumerable<string> errorMessages)
        {
            var serviceOutput = new ServiceOutput<TServiceOutputBase>();
            serviceOutput.ErrorMessages.AddRange(errorMessages);

            return serviceOutput;
        }

        public static ServiceOutput<TServiceOutputBase> Fail(IEnumerable<string> errorMessages, TServiceOutputBase output)
        {
            var serviceOutput = new ServiceOutput<TServiceOutputBase> {Output = output};
            serviceOutput.ErrorMessages.AddRange(errorMessages);

            return serviceOutput;
        }
    }
}