﻿using System.Collections.Generic;
using TestC2P2.DataAccess.Entities;
using TestC2P2.Services.Models.Output.Base;

namespace TestC2P2.Services.Models.Output
{
    public class GetTransactionsOutput : ServiceOutputBase
    {
        public IEnumerable<Transaction> Transactions { get; set; }
    }
}