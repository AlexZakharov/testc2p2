﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CsvHelper;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using TestC2P2.Common.Helpers.Interfaces;
using TestC2P2.Common.Models.Parser;
using TestC2P2.Common.Models.Transaction;
using TestC2P2.Services.Interfaces;

namespace TestC2P2.Services.Classes
{
    public class CsvTransactionFileParser : ICsvTransactionFileParser
    {
        private const string ErrorOnParsingErrorMessage = "Someting went wrong";

        private readonly IStatusHelper _statusHelper;
        private readonly ILogger<CsvTransactionFileParser> _logger;

        public CsvTransactionFileParser(IStatusHelper statusHelper, ILogger<CsvTransactionFileParser> logger)
        {
            _statusHelper = statusHelper;
            _logger = logger;
        }
        
        // TODO need refactoring
        public IEnumerable<TransactionViewModel> ParseFile(IFormFile file)
        {
            try
            {
                var transactionViewModels = new List<TransactionViewModel>();
                var csvTransactionReadModels = new List<CsvTransactionReadModel>();
                using (var readStream = file.OpenReadStream())
                {
                    using (var streamReader = new StreamReader(readStream))
                    {
                        using (var csvReader = new CsvReader(streamReader))
                        {
                            csvReader.Configuration.Delimiter = ",";
                            csvReader.Configuration.MissingFieldFound = null;
                            csvReader.Configuration.BadDataFound = null;

                            while (csvReader.Read())
                            {
                                var record = csvReader.GetRecord<CsvTransactionReadModel>();
                                csvTransactionReadModels.Add(record);
                            }
                        }
                    }
                }

                if (csvTransactionReadModels.Any())
                {
                    transactionViewModels.AddRange(csvTransactionReadModels.Select(csvTransactionReadModel => new TransactionViewModel
                    {
                        TransactionId = csvTransactionReadModel.TransactionId,
                        Amount = csvTransactionReadModel.Amount,
                        CurrencyCode = csvTransactionReadModel.CurrencyCode,
                        TransactionDate = csvTransactionReadModel.TransactionDate,
                        Status = _statusHelper.GetStatusEnumByInputSting(csvTransactionReadModel.Status)
                    }));
                }

                return transactionViewModels;
            }
            catch (Exception exception)
            {
                _logger.LogError($"Log errors: {exception.Message}");
                throw new Exception(ErrorOnParsingErrorMessage);
            }
        }
    }
}