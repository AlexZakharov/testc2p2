﻿using TestC2P2.Common.Enums;
using TestC2P2.Services.Interfaces;

namespace TestC2P2.Services.Classes
{
    public class FileParserTransactionFabric : IFileParserTransactionFabric
    {
        private readonly ICsvTransactionFileParser _csvTransactionFileParser;
        private readonly IXmlTransactionFileParser _xmlTransactionFileParser;

        public FileParserTransactionFabric(
            ICsvTransactionFileParser csvTransactionFileParser,
            IXmlTransactionFileParser xmlTransactionFileParser)
        {
            _csvTransactionFileParser = csvTransactionFileParser;
            _xmlTransactionFileParser = xmlTransactionFileParser;
        }

        public ITransactionFileParser GetTransactionFileParser(SupportedFileTypesEnum? fileType)
        {
            switch (fileType)
            {
                case SupportedFileTypesEnum.Csv:
                    return _csvTransactionFileParser;
                
                case SupportedFileTypesEnum.Xml:
                    return _xmlTransactionFileParser;
                
                default:
                    return null;
            }
        }
    }
}