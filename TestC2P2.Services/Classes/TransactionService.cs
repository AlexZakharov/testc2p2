﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Logging;
using TestC2P2.Common.Models.Transaction;
using TestC2P2.DataAccess.Entities;
using TestC2P2.DataAccess.UnitOfWork.Interfaces;
using TestC2P2.Services.Interfaces;
using TestC2P2.Services.Models.Input;
using TestC2P2.Services.Models.Output;
using TestC2P2.Services.Validators.Interfaces;

namespace TestC2P2.Services.Classes
{
    public class TransactionService : ITransactionService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IFileParserTransactionFabric _fileParserTransactionFabric;
        private readonly ITransactionViewModelValidator _transactionViewModelValidator;
        private readonly ILogger<TransactionService> _logger;

        public TransactionService(
            IUnitOfWork unitOfWork,
            IMapper mapper,
            IFileParserTransactionFabric fileParserTransactionFabric,
            ITransactionViewModelValidator transactionViewModelValidator,
            ILogger<TransactionService> logger)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _fileParserTransactionFabric = fileParserTransactionFabric;
            _transactionViewModelValidator = transactionViewModelValidator;
            _logger = logger;
        }

        public async Task<ServiceOutput<ImportTransactionFromFileOutput>> ImportTransactionFromFile(ImportTransactionFromFileInput importTransactionFromFileInput)
        {
            ServiceOutput<ImportTransactionFromFileOutput> importTransactionFromFileOutput;
            
            try
            {
                var transactionFileParser = _fileParserTransactionFabric.GetTransactionFileParser(importTransactionFromFileInput.FileTypeEnum);

                var transactionViewModels = transactionFileParser.ParseFile(importTransactionFromFileInput.File).ToList();

                if (transactionViewModels.Any())
                {
                    var validationErrors = new List<string>();
                    var recordCounter = 1;
                    
                    foreach (var transactionViewModel in transactionViewModels)
                    {
                        var transactionViewModelValidatorOutput = _transactionViewModelValidator
                            .Validate(new TransactionViewModelValidatorInput {TransactionViewModel = transactionViewModel});

                        if (transactionViewModelValidatorOutput.Failed)
                        {
                            validationErrors.Add($"Record #{recordCounter}: {string.Join(", ", transactionViewModelValidatorOutput.ErrorMessages)}");
                        }

                        recordCounter++;
                    }

                    if (!validationErrors.Any())
                    {
                        var transactions = _mapper.Map<List<Transaction>>(transactionViewModels);

                        using (var unitOfWork = _unitOfWork)
                        {
                            foreach (var transaction in transactions)
                            {
                                await unitOfWork.TransactionRepository.AddOrUpdateAsync(transaction).ConfigureAwait(false);
                            }

                            await unitOfWork.Save();
                        }
                    
                        importTransactionFromFileOutput = ServiceOutput<ImportTransactionFromFileOutput>.Success();
                    }
                    else
                    {
                        _logger.LogError($"Log errors: {string.Join(", ", validationErrors)}");
                        importTransactionFromFileOutput = ServiceOutput<ImportTransactionFromFileOutput>.Fail(validationErrors);
                    }
                }
                else
                {
                    importTransactionFromFileOutput = ServiceOutput<ImportTransactionFromFileOutput>.Success();
                }
            }
            catch (Exception exception)
            {
                importTransactionFromFileOutput = ServiceOutput<ImportTransactionFromFileOutput>.Fail(exception.Message);
            }

            return importTransactionFromFileOutput;
        }

        public async Task<ServiceOutput<GetTransactionsOutput>> GetTransactions(GetTransactionsInput getTransactionsInput)
        {
            using (var unitOfWork = _unitOfWork)
            {
                ServiceOutput<GetTransactionsOutput> getTransactionsOutput;
                
                try
                {
                    var transactionSearchModel = _mapper.Map<TransactionSearchModel>(getTransactionsInput);
                    var transactions = await unitOfWork.TransactionRepository.GetAllBySearchModel(transactionSearchModel);
                    getTransactionsOutput = ServiceOutput<GetTransactionsOutput>.Success(new GetTransactionsOutput{ Transactions = transactions});
                }
                catch (Exception exception)
                {
                    getTransactionsOutput = ServiceOutput<GetTransactionsOutput>.Fail(exception.Message);
                }
                
                return getTransactionsOutput;
            }
        }
    }
}