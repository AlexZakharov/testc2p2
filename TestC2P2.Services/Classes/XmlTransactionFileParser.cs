﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using TestC2P2.Common.Helpers.Interfaces;
using TestC2P2.Common.Models.Transaction;
using TestC2P2.Services.Interfaces;

namespace TestC2P2.Services.Classes
{
    public class XmlTransactionFileParser : IXmlTransactionFileParser
    {
        private const string ErrorOnParsingErrorMessage = "Someting went wrong";
        
        private readonly IStatusHelper _statusHelper;
        private readonly ILogger<XmlTransactionFileParser> _logger;

        public XmlTransactionFileParser(IStatusHelper statusHelper, ILogger<XmlTransactionFileParser> logger)
        {
            _statusHelper = statusHelper;
            _logger = logger;
        }
        
        // TODO need refactoring
        public IEnumerable<TransactionViewModel> ParseFile(IFormFile file)
        {
            try
            {
                using (var readStream = file.OpenReadStream())
                {
                    XDocument xDocument = XDocument.Load(readStream);
                    IEnumerable<TransactionViewModel> transactionViewModels = xDocument
                        .Descendants("Transaction")
                        .Select(transaction =>
                            new TransactionViewModel
                            {
                                TransactionId = transaction.Attribute("id")?.Value,
                                Amount = Convert.ToDecimal(transaction.Element("PaymentDetails")?.Element("Amount")?.Value),
                                CurrencyCode = transaction.Element("PaymentDetails")?.Element("CurrencyCode")?.Value,
                                TransactionDate = Convert.ToDateTime(transaction.Element("TransactionDate")?.Value),
                                Status = _statusHelper.GetStatusEnumByInputSting(transaction.Element("Status")?.Value)
                            }).ToList();
                    return transactionViewModels;
                }
            }
            catch (Exception exception)
            {
                _logger.LogError($"Log errors: {exception.Message}");
                throw new Exception(ErrorOnParsingErrorMessage);
            }
        }
    }
}