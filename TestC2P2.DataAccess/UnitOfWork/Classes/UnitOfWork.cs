using System;
using System.Threading.Tasks;
using TestC2P2.DataAccess.Contexts;
using TestC2P2.DataAccess.Repositories;
using TestC2P2.DataAccess.UnitOfWork.Interfaces;

namespace TestC2P2.DataAccess.UnitOfWork.Classes
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _dbContext;
        private TransactionRepository _transactionRepository;

        public UnitOfWork(ApplicationDbContext db)
        {
            _dbContext = db;
        }

        public TransactionRepository TransactionRepository => _transactionRepository ?? (_transactionRepository = new TransactionRepository(_dbContext));
        
        public async Task<bool> Save()
        {
            return await _dbContext.SaveChangesAsync() > 0;
        }

        private bool _disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed) return;
            if (disposing)
            {
                _dbContext.Dispose();
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}