using System;
using System.Threading.Tasks;
using TestC2P2.DataAccess.Entities;
using TestC2P2.DataAccess.Repositories;

namespace TestC2P2.DataAccess.UnitOfWork.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        TransactionRepository TransactionRepository { get; }

        Task<bool> Save();
    }
}