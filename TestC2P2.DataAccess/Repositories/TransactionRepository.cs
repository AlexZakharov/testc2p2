﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TestC2P2.Common.Models.Transaction;
using TestC2P2.DataAccess.Contexts;
using TestC2P2.DataAccess.Entities;
using TestC2P2.DataAccess.Repositories.Base.Classes;

namespace TestC2P2.DataAccess.Repositories
{
    public class TransactionRepository : BaseRepository<Transaction>
    {
        private readonly ApplicationDbContext _dbContext;

        public TransactionRepository(ApplicationDbContext context) : base(context)
        {
            _dbContext = context;
        }
        
        public async Task AddOrUpdateAsync(Transaction transaction)
        {
            var existTransaction = await _dbContext.Transactions.FirstOrDefaultAsync(t => t.TransactionId == transaction.TransactionId);
            if (existTransaction != null)
            {
                transaction.Id = existTransaction.Id;
                _dbContext.Entry(existTransaction).CurrentValues.SetValues(transaction);
            }
            else
            {
                await this.AddAsync(transaction);
            }
        }
        
        public async Task<IEnumerable<Transaction>> GetAllBySearchModel(TransactionSearchModel transactionSearchModel)
        {
            var query = _dbContext.Transactions.Select(cp => cp);
            
            if (!string.IsNullOrEmpty(transactionSearchModel.Currency))
                query = query.Where(transaction => transaction.CurrencyCode.ToLower().Contains(transactionSearchModel.Currency.ToLower()));
            
            if (transactionSearchModel.StartDate != null && transactionSearchModel.EndDate != null)
                query = query.Where(transaction => transaction.TransactionDate >= transactionSearchModel.StartDate && transaction.TransactionDate <= transactionSearchModel.EndDate);
            
            if (transactionSearchModel.Status != null)
                query = query.Where(transaction => transaction.Status == transactionSearchModel.Status);
            
            return await query.AsNoTracking().ToListAsync();
        }
    }
}