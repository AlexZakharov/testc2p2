using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TestC2P2.DataAccess.Repositories.Base.Interfaces
{
    public interface IBaseRepository<TEntity> where TEntity : class
    {
        Task AddAsync(TEntity entity);
    }
}