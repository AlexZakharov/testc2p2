using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TestC2P2.DataAccess.Contexts;
using TestC2P2.DataAccess.Repositories.Base.Interfaces;

namespace TestC2P2.DataAccess.Repositories.Base.Classes
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class
    {
        private readonly ApplicationDbContext _dbContext;

        private readonly DbSet<TEntity> _dbSet;

        protected BaseRepository(ApplicationDbContext context)
        {
            _dbContext = context;
            _dbSet = _dbContext.Set<TEntity>();
        }
        
        public virtual async Task AddAsync(TEntity entity)
        {
            await _dbSet.AddAsync(entity);
        }
    }
}