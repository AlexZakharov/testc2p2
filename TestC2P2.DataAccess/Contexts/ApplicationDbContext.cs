using Microsoft.EntityFrameworkCore;
using TestC2P2.DataAccess.Entities;

namespace TestC2P2.DataAccess.Contexts
{
    public class ApplicationDbContext : DbContext
    {
        
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {}
        
        public DbSet<Transaction> Transactions { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Transaction>()
                .HasIndex(transaction => new {transaction.TransactionId}).IsUnique();
        }
    }
}