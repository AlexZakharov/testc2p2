using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TestC2P2.Common.Enums;

namespace TestC2P2.DataAccess.Entities
{
    [Table("Transaction")]
    public class Transaction
    {
        [Key]
        public long Id { get; set; }
        
        [Required]
        [StringLength(50)]
        public string TransactionId { get; set; }
        
        [Required]
        [DataType(DataType.Currency)]
        public decimal Amount { get; set; }
        
        [Required]
        [StringLength(3)]
        public string CurrencyCode { get; set; }
        
        [Required]
        public DateTime TransactionDate { get; set; }
        
        [Required]
        public StatusEnum Status { get; set; }
    }
}